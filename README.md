# Tutorials for the MCnet Summer School Dresden (2021)

[WEBSITE](https://indico.cern.ch/event/1005703/)

For the first afternoon (Wednesday 08 Sep), choose from the following tutorials:
 * [Herwig7](herwig)
 * [Madgraph](madgraph) (also on Monday 13 Sep)
 * [Pythia8](pythia)
 * [Sherpa](sherpa)
 * [Rivet](rivet)

For the remaining afternoons (Monday 13 Sep and Wednesday 15 Sep), choose from the following tutorials:
 * [Event generation for Heavy Ion collisions](heavy-ions) 
 * [Constraining new physics models using Contur](contur) 
 * [Constructing your own parton shower in Python](parton-shower)

## Prerequisites

Most tutorials will be based on Docker.
If you are using MacOS or Windows, you will first need to create a DockerID at https://hub.docker.com/signup

Head to https://docs.docker.com/install for installation instructions.

You can check that Docker has installed properly by running the `hello-world` Docker image

        $ docker run hello-world

Some helpful commands:

`docker run [OPTIONS] IMAGE` to run an image; 
`docker ps` to list active containers; 
`docker image ls` to list available images/apps; 
`docker attach [OPTIONS] CONTAINER` to attach to a container

When you are running inside a container, you can use `CTLR-p CTLR-q` to detach from it and leave it running. 

Please see tutorial-specific instructions as well.


